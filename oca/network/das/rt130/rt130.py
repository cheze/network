# -*- coding: utf-8 -*-
from datetime import datetime
from crc16 import CRC16
import struct
import socket
import re


def lat_conversion(data):
    c = -1 if data[0] == 'S' else 1
    return c * int(data[2:4]) + float(data[5:])/60

def lon_conversion(data):
    c = -1 if data[0] == 'W' else 1
    return c * int(data[1:4]) + float(data[5:])/60


'''
RT130_COMMAND_DEF data structure definition :

'<cmd_code>': {
  'payload': '<packet_payload>',
  'response': [
    {
      'type': <'normal'|'list'>,
      'name': <for_type_list_only>,
      'size': <name_of_field_that_contain_size>,
      'regexp': <re.compile object>,
      'field': [
        {'name': '<field_name>', 'conversion': <conversion_function>},
        ...
      ],
    },
    ...
  }
}
'''

RT130_COMMAND_DEF = {
  'PR_PC': {
    'payload': 'PRPC %(channel_number)sPR',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'PC[\d ]{2}([\d ]{2})(.{10})(.{10})(.{10})(.{10})(.{10})(.{10})(.{4})(.{4})([\d ]{4})(.{12})(.{12})(.{40})'),
        'field': [
          {'name': 'channel_number', 'conversion': int},
          {'name': 'channel_name', 'conversion': lambda x: x.strip()},
          {'name': 'azimuth', 'conversion': lambda x: x.strip()},
          {'name': 'incline', 'conversion': lambda x: x.strip()},
          {'name': 'location_x', 'conversion': lambda x: x.strip()},
          {'name': 'location_y', 'conversion': lambda x: x.strip()},
          {'name': 'location_z', 'conversion': lambda x: x.strip()},
          {'name': 'unit_xy', 'conversion': lambda x: x.strip()},
          {'name': 'unit_z', 'conversion': lambda x: x.strip()},
          {'name': 'gain', 'conversion': int},
          {'name': 'sensor_model', 'conversion': lambda x: x.strip()},
          {'name': 'serial_number', 'conversion': lambda x: x.strip()},
          {'name': 'comment', 'conversion': lambda x: x.strip()}
        ]
      }
    ]
  },
  'PR_PD': {
    'payload': 'PRPD %(stream_number)sPR',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'PD[\d ]{2}([1-8 ]{2})(.{16})(.)(.)(.)(.)    (.{16})([\d. ]{4})(16|32|CO|C2)(.{4})(.{162})'),
        'field': [
          {'name': 'stream_number', 'conversion': int},
          {'name': 'stream_name', 'conversion': lambda x: x.strip()},
          {'name': 'rec_ram', 'conversion': lambda x: x != ' '},
          {'name': 'rec_disk', 'conversion': lambda x: x != ' '},
          {'name': 'rec_eth', 'conversion': lambda x: x != ' '},
          {'name': 'rec_serial', 'conversion': lambda x: x != ' '},
          {'name': 'channel', 'conversion': lambda x: map(lambda y: y != ' ', x)},
          {'name': 'sample_rate', 'conversion': float},
          {'name': 'data_format', 'conversion': str},
          {'name': 'trigger_type', 'conversion': lambda x: x.strip()},
          {'name': 'trigger', 'conversion': lambda x: x.strip()}
        ]
      }
    ]
  },
  'PR_PS': {
    'payload': 'PRPS  PR',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'PS[\d ]{2}([\d ]{2})(.{24})(.{40})(.{4})(.{24})(.{40})'),
        'field': [
          {'name': 'experiment_number', 'conversion': lambda x: x.strip()},
          {'name': 'experiment_name', 'conversion': lambda x: x.strip()},
          {'name': 'experiment_comment', 'conversion': lambda x: x.strip()},
          {'name': 'station_number', 'conversion': lambda x: x.strip()},
          {'name': 'station_name', 'conversion': lambda x: x.strip()},
          {'name': 'station_comment', 'conversion': lambda x: x.strip()}
        ]
      }
    ]
  },
  'SS_AD': {
    'payload': 'SSAD' + ' ' * 14 + 'SS',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'AD.{18}([0-4]) '),
        'field': [
          {'name': 'sensor_count', 'conversion': int}
        ]
      },
      {
        'type': 'list',
        'name': 'sensor',
        'size': 'sensor_count',
        'regexp': re.compile(r'(\d) ([\d ]{6})([\d ]{6})([\d. ]{4})([\d. +-]{4})([\d. +-]{4})([\d. +-]{4})'),
        'field': [
          {'name': 'sensor_number', 'conversion': int},
          {'name': 'count', 'conversion': int},
          {'name': 'count_limit', 'conversion': int},
          {'name': 'level', 'conversion': float},
          {'name': 'aux1', 'conversion': float},
          {'name': 'aux2', 'conversion': float},
          {'name': 'aux3', 'conversion': float}
        ]
      }

    ]
  },
  'SS_AQ': {
    'payload': 'SSAQ' + ' ' * 14 + 'SS',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'AQ.{18}(Y|N)(Y|N)([\d ]{6})(Y|N)([\d ]{6})([\d ]{6})([\d ]{6})'),
        'field': [
          {'name': 'acquisition_requested', 'conversion': lambda x: x == 'Y'},
          {'name': 'acquisition_active', 'conversion': lambda x: x == 'Y'},
          {'name': 'event_count', 'conversion': int},
          {'name': 'event_in_progress', 'conversion': lambda x: x == 'Y'},
          {'name': 'ram_total', 'conversion': int},
          {'name': 'ram_used', 'conversion': int},
          {'name': 'ram_available', 'conversion': int}
        ]
      }
    ]
  },
  'SS_CH': {
    'payload': 'SSCH%(group_name)s' + ' ' * 13 + 'SS',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'CH.{18}([1-4]) ([\d]{6})([0-9A-Fa-f ]{8})(\d{4})([\d.]{5}) (.{2})([\d]{6})([0-9A-Fa-f ]{8})(\d{4})([\d.]{5})(.{2}) ([\d]{6})([0-9A-Fa-f ]{8})(\d{4})([\d.]{5}) (.{2})'),
        'field': [
          {'name': 'channel_group', 'conversion': int},
          {'name': 'c1_full_scale_analog', 'conversion': float},
          {'name': 'c1_full_scale_digital', 'conversion': lambda x: int(x, 16)},
          {'name': 'c1_gain', 'conversion': float},
          {'name': 'c1_lsb_value', 'conversion': float},
          {'name': 'c1_lsb_unit', 'conversion': str},
          {'name': 'c2_full_scale_analog', 'conversion': float},
          {'name': 'c2_full_scale_digital', 'conversion': lambda x: int(x, 16)},
          {'name': 'c2_gain', 'conversion': float},
          {'name': 'c2_lsb_value', 'conversion': float},
          {'name': 'c2_lsb_unit', 'conversion': str},
          {'name': 'c3_full_scale_analog', 'conversion': float},
          {'name': 'c3_full_scale_digital', 'conversion': lambda x: int(x, 16)},
          {'name': 'c3_gain', 'conversion': float},
          {'name': 'c3_lsb_value', 'conversion': float},
          {'name': 'c3_lsb_unit', 'conversion': str}
        ]
      }
    ]
  },
  'SS_DK': {
    'payload': 'SSDK' + ' ' * 14 + 'SS',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'DK.{18}([\d .]{6})([\d .]{6})([\d .]{6})([\d .]{6})([\d .]{6})([\d .]{6})(1|2)(Y|N)([0-9A-Fa-f ]{2})'),
        'field': [
          {'name': 'disk_1_total', 'conversion': float},
          {'name': 'disk_1_used', 'conversion': float},
          {'name': 'disk_1_available', 'conversion': float},
          {'name': 'disk_2_total', 'conversion': float},
          {'name': 'disk_2_used', 'conversion': float},
          {'name': 'disk_2_available', 'conversion': float},
          {'name': 'current_disk', 'conversion': int},
          {'name': 'wrap_enabled', 'conversion': lambda x: x == 'Y'},
          {'name': 'wrap_count', 'conversion': lambda x: int(x, 16)}
        ]
      }
    ]
  },
  'SS_PR': {
    'payload': 'SSPR' + ' ' * 14 + 'SS',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'PR.{18}([0-9 ]{2})(8)(2)    '),
        'field': [
          {'name': 'max_channel', 'conversion': int},
          {'name': 'max_datastream', 'conversion': int},
          {'name': 'max_network_port', 'conversion': int}
        ]
      },
      {
        'type': 'list',
        'size': 'max_channel',
        'name': 'active_channel',
        'regexp': re.compile('(.)'),
        'field': [
          {'name': 'flag', 'conversion': lambda x: x!= ' '}
        ]
      },
      {
        'type': 'list',
        'size': 'max_datastream',
        'name': 'active_datastream',
        'regexp': re.compile('(.)'),
        'field': [
          {'name': 'flag', 'conversion': lambda x: x!= ' '}
        ]
      }
    ]
  },
  'SS_SI': {
    'payload': 'SSSI' + ' ' * 14 + 'SS',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'SI.{18}([1-4]) (.{30})(.{14})(.{14})([\d ]{2})'),
        'field': [
          {'name': 'sensor_number', 'conversion': int},
          {'name': 'manufacturer', 'conversion': lambda x: x.strip()},
          {'name': 'model', 'conversion': lambda x: x.strip()},
          {'name': 'serial_number', 'conversion': lambda x: x.strip()},
          {'name': 'number_of_component', 'conversion': int}
        ]
      },
      {
        'type': 'list',
        'name': 'component_desc',
        'size': 'number_of_component',
        'regexp': re.compile(r'([\d ]{2})(.{4})(.{10})([\d .]{14})'),
        'field': [
          {'name': 'component_number', 'conversion': int},
          {'name': 'orientation', 'conversion': str},
          {'name': 'unit', 'conversion': str},
          {'name': 'volt_per_unit', 'conversion': float}
        ]
      }
    ]
  },
  'SS_US': {
    'payload': 'SSUS' + ' ' * 14 + 'SS',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'US.{18}([0-9.]{4})([0-9.]{4})([0-9.+\- ]{6})([0-9.]{4})SS$'),
        'field': [
          {'name': 'input_power', 'conversion': float},
          {'name': 'backup_power', 'conversion': float},
          {'name': 'temperature', 'conversion': float},
          {'name': 'charger_power', 'conversion': float}
        ]
      }
    ]
  },
  'SS_VS': {
    'payload': 'SSVS' + ' ' * 14 + 'SS',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'VS.{18}([\w. ]{16})([\d ]{2})'),
        'field': [
          {'name': 'cpu_version', 'conversion': lambda x: x.strip()},
          {'name': 'board_count', 'conversion': int}
        ]
      },
      {
        'type': 'list',
        'regexp': re.compile(r'([\d ]{4})(.)(.{3})([\d ]{4})([\d ]{4})(.)(.{3})'),
        'name': 'board_info',
        'size': 'board_count',
        'field': [
            {'name': 'number', 'conversion': int},
            {'name': 'revision', 'conversion': str},
            {'name': 'acronym', 'conversion': str},
            {'name': 'serial_number', 'conversion': int},
            {'name': 'FPGA_number', 'conversion': int},
            {'name': 'FPGA_min_rev', 'conversion': str},
            {'name': 'FPGA_version', 'conversion': str}
        ]
      }
    ]
  },
  'SS_XC': {
    'payload': 'SSXC' + ' ' * 14 + 'SS',
    'response': [
      {
        'type': 'normal',
        'regexp': re.compile(r'XC.{18}(.{8})([\d, +-]{11})(L|U)([\d ]{2})(.{12})(.{12})([\d +-]{6})(Y|N)(C|D|O)'),
        'field': [
          {'name': 'last_lock', 'conversion': lambda x: int(x[:2])*86400 + int(x[3:5])*60 + int(x[6:])},
          {'name': 'last_lock_phase', 'conversion': lambda x: float('%s.%s%s' % (x[:2], x[4:7], x[8:]))},
          {'name': 'lock_status', 'conversion': str},
          {'name': 'nb_satellite', 'conversion': int},
          {'name': 'latitude', 'conversion': lat_conversion},
          {'name': 'longitude', 'conversion': lon_conversion},
          {'name': 'altitude', 'conversion': int},
          {'name': 'gps_on', 'conversion': lambda x: x == 'Y'},
          {'name': 'gps_mode', 'conversion': str}
        ]
      }
    ]
  }
}


class RT130(object):
    def __init__(self, host, port=5000):
        self.__host = host
        self.__port = port
        self.__connected = False
        self.__crc_manager = CRC16(modbus_flag=True)

    def _connect(self):
        if self.__connected:
            return
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__sock.settimeout(30)
        self.__sock.connect((self.__host, self.__port))

    def _check_crc(self, content, crc):
        expected_crc = '%04X' % self.__crc_manager.calculate(content)
        return expected_crc == crc

    def _decode_response(self, cmd_code, data):
        #print(data)

        def decode_fields(result_dict, resp_def, content):
            m = current['regexp'].match(content)
            if not m:
                raise ValueError('Invalid packet structure')
            values = m.groups()
            for value, data_fmt in zip(values, resp_def['field']):
                result_dict[data_fmt['name']] = data_fmt['conversion'](value)
            return m.end(0)

        resp_def = RT130_COMMAND_DEF[cmd_code]
        unit_id = data[2:6]
        crc = data[-6:-2]
        resp_content = data[12:-6]
        if not self._check_crc(data[2:-6], crc):
            raise ValueError('CRC check failed')
        index = 0
        result = {'unit_id': unit_id}
        for current in resp_def['response']:
            if current['type'] == 'normal':
                index += decode_fields(result, current, resp_content[index:])
            elif current['type'] == 'list':
                result[current['name']] = []
                for i in range(result[current['size']]):
                    current_result = {}
                    index += decode_fields(current_result, current, resp_content[index:])
                    result[current['name']].append(current_result)
            else:
                raise ValueError('type %s not implemented !!!' % current['type'])
        return result

    def _send_command(self, cmd_code, parameter={}):
        '''
        Send command according to given code

        cmd_code -- command code defined in RT130_COMMAND_DEF

        return  -- key/value dictionary
        '''
        self._connect()
        payload = RT130_COMMAND_DEF[cmd_code]['payload'] % parameter
        content = '0000%(length)04d%(payload)s' % {
          'length': len(payload) + 6,
          'payload': payload
        }

        packet = '\x84\x00%(content)s%(crc)04X\r\n' % {
          'content': content,
          'crc': self.__crc_manager.calculate(content)
        }
        #print(packet)
        self.__sock.sendall(packet)
        data = self.__sock.recv(4096)
        return self._decode_response(cmd_code, data)

    def get_parameter_channel(self, channel_number):
        try:
            return self._send_command('PR_PC', {'channel_number': channel_number})
        except ValueError:
            raise ValueError('No response for channel number %s' % channel_number)

    def get_parameter_stream(self, stream_number):
        try:
            return self._send_command('PR_PD', {'stream_number': stream_number})
        except ValueError:
            raise ValueError('No response for stream number %s' % stream_number)

    def get_parameter_station(self):
        return self._send_command('PR_PS')

    def get_status_auxiliary_data(self):
        return self._send_command('SS_AD')

    def get_status_acquisition(self):
        return self._send_command('SS_AQ')

    def get_status_channel_input(self, group_name):
        return self._send_command('SS_CH', {'group_name': group_name})

    def get_status_disk(self):
        return self._send_command('SS_DK')

    def get_status_parameter(self):
        return self._send_command('SS_PR')

    def get_status_sensor_info(self):
        return self._send_command('SS_SI')

    def get_status_unit(self):
        return self._send_command('SS_US')

    def get_status_version(self):
        return self._send_command('SS_VS')

    def get_status_external_clock(self):
        return self._send_command('SS_XC')


if __name__ == '__main__':
    import json
    my_reftek = RT130('10.254.84.75')
    print(json.dumps(my_reftek.get_parameter_channel(2), indent=2))
    #print(json.dumps(my_reftek.get_parameter_stream(1), indent=2))
    #print(json.dumps(my_reftek.get_parameter_station(1), indent=2))
    #print(json.dumps(my_reftek.get_status_auxiliary_data(), indent=2))
    #print(json.dumps(my_reftek.get_status_acquisition(), indent=2))
    #print(json.dumps(my_reftek.get_status_channel_input(), indent=2))
    #print(json.dumps(my_reftek.get_status_disk(), indent=2))
    #print(json.dumps(my_reftek.get_status_parameter(), indent=2))
    #print(json.dumps(my_reftek.get_status_sensor_info(), indent=2))
    #print(json.dumps(my_reftek.get_status_unit(), indent=2))
    #print(json.dumps(my_reftek.get_status_version(), indent=2))
    #print(json.dumps(my_reftek.get_status_external_clock(), indent=2))
