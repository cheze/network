#!/usr/bin/python
# -*- coding: utf-8 -*-
from datetime import datetime
from struct import Struct

CRC_POLYNOMIAL = 1443300200

QDP_VERSION = 2


class AttributeDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__


EPOCH = int(datetime(2000, 1, 1, 0, 0, 0).strftime('%s'))


STRUCTURE = AttributeDict({
    'QDP_HEADER':       Struct('>IBBHHH'),
    'STATUS_GLOBAL':    Struct('>HHHHIIIIIIHHHHHHHHI'),
    'STATUS_GPS':       Struct('>HHHH10s12s6s12s14s14sII'),
    'STATUS_PWR':       Struct('>HhHHHHhHHBB'),
    'STATUS_BOOM':      Struct('>hhhhhhHHHhHHhhI'),
    'STATUS_THREAD1':   Struct('>HHIHH'),
    'STATUS_THREAD2':   Struct('>IHBBIHH'),
    'STATUS_PLL':       Struct('>ffffIIhH'),
    'STATUS_SAT1':      Struct('>HH'),
    'STATUS_SAT2':      Struct('>HhhH'),
    'STATUS_ARP1':      Struct('>HH'),
    'STATUS_ARP2':      Struct('>BBBBBBBBBBH'),
    'PING_STATUS_REQ':  Struct('>HHI'),
    'PING_STATUS_RESP': Struct('>HHHHIIII'),
    'PING_INFO_RESP':   Struct('>HHHHIQIIIIIIIIHHHHHHHHHH'),
    'C1_MYSN':          Struct('>QII'),
    'C1_PHY':           Struct('>QIIIIIHHHHHHHHHHHHHH'),
    'C1_RQSRV':         Struct('>Q'),
    'C1_DSRV':          Struct('>Q'),
    'C1_SRVCH':         Struct('>QIHH'),
    'C1_SRVRSP':        Struct('>QQIHHQQQ'),
    'C1_LOG':           Struct('>HHHHHHHHHHHHHHHHHHIHHHHI'),
    'C1_RQLOG':         Struct('>h'),
    'C1_FIX':           Struct('>IIII'+10*'H'+'I'+4*'Q'+11*'I'+8*'B'+16*'I'),
    'C1_RQMEM':         Struct('>IHHQQ'),
    'C1_MEM':           Struct('>IHHHH'),
    'TF_DSS':           Struct('>8s8s8sIIBBHHH'),
    'TF_CLOCK':         Struct('>IH'+8*'B'+'H'),
    'TF_MT':            Struct('>2s3s2s3s'),
    'TF_CFG':           Struct('>2s3sBH'),
    'T1_LCQ':           Struct('>2s3sBBBIH')
})

TYPE = AttributeDict({
    'BYTE': Struct('>B'),
    'WORD': Struct('>H'),
    'LONGWORD': Struct('>I')
})

LCQ_OPT = AttributeDict({
    'LO_EVENT': 1, # Event
    'LO_DETP': 2, # Write Detector Packets
    'LO_CALP': 4, # Write Calibration Packets
    'LO_PEB': 8, # Pre Event Buffers
    'LO_GAP': 0x10, # Gap Override
    'LO_CALDLY': 0x20, # Calibration Delay
    'LO_FRAME': 0x40, # Frame Count
    'LO_FIRMULT': 0x80, # FIR Filter Multiplier
    'LO_AVG': 0x100, # Averaging Parameters
    'LO_CDET': 0x200, # Control Detector
    'LO_DEC': 0x400, # Decimation
    'LO_NOUT': 0x800, # No Output
    'LO_DET1': 0x1000, # Detector 1
    'LO_DET2': 0x2000, # Detector 2
    'LO_DET3': 0x4000, # Detector 3
    'LO_DET4': 0x8000, # Detector 4
    'LO_DET5': 0x10000, # Detector 5
    'LO_DET6': 0x20000, # Detector 6
    'LO_DET7': 0x40000, # Detector 7
    'LO_DET8': 0x80000, # Detector 8
    'LO_NSEVT': 0x8000000, # Netserv is event only
    'LO_DOFF': 0x10000000, # Don't generate data
    'LO_DATAS': 0x20000000, # Enable writing to dataserv
    'LO_NETS': 0x40000000, # Enable writing to netserv
    'LO_CNPP': 0x80000000 # Preserve CNP timetags
})

CONFIG_TOKEN = AttributeDict({
    'TF_NOP': 0, # nothing
    'TF_VERSION': 1, # Token Version Number
    'TF_NET_STAT': 2, # Network and Station ID
    'TF_NETSERV': 3, # Netserver Port
    'TF_DSS': 4, # DSS Parameters
    'TF_WEB': 5, # DP Webserver
    'TF_CLOCK': 6, # Clock Processing
    'TF_MT': 7, # Log and Timing record ID's
    'TF_CFG': 8, # How to write configuration information
    'TF_DATASERV': 9, # Dataserver Port
    # 8 Bit Length Field
    'T1_LCQ': 128, # Logical Channel Queue
    'T1_IIR': 129, # IIR Filter
    'T1_FIR': 130, # FIR Filter
    'T1_CTRL': 131, # Control Detector
    'T1_MHD': 132, # Murdock Hutt Detector
    'T1_THRD': 133, # Threshold Detector
    'T1_NONCOMP': 134, # Non-compliant DP
    'T1_DPLCQ': 135, # DP Logical Channel Queue
    # 16 bit Length Field
    'T2_CNAMES': 192, # Comm Event Names
    'T2_ALERT': 193, # Email Alert
    'T2_OPAQUE': 194, # Opaque Configuration
    'OVERHEAD': 10 # Structure overhead
})

DATA_CH = AttributeDict({
    'READ_PREV_STREAM': 0, # Decimated
    'MESSAGE_STREAM': 1, # Message LCQ
    'TIMING_STREAM': 2, # Timing LCQ
    'CFG_STREAM': 3, # Configuration LCQ
    'DCM_ST': 0xE0, # Status mask
    'DCM': 0xF8, # Mask for all others
    'DC_ST38': 0x00, # Status - 3 x 8 bit parameters
    'DC_ST816': 0x20, # Status - 8 and 16
    'DC_ST32': 0x40, # Status - 8, 16, and 32
    'DC_ST232': 0x60, # Status - 8, 16, and 2 X 32
    'DC_MN38': 0x80, # Main data sample - 3 x 8
    'DC_MN816': 0x88, # Main - 8 and 16
    'DC_MN32': 0x90, # Main - 8, 16, and 32
    'DC_MN232': 0x98, # Main - 8, 16, and 2 X 32
    'DC_AG38': 0xA0, # Analog sample - 3 x 8
    'DC_AG816': 0xA8, # Analog sample - 8 and 16
    'DC_AG32': 0xB0, # Analog sample - 8, 16, and 32
    'DC_AG232': 0xB8, # Analog sample - 8, 16, and 2 x 32
    'DC_CNP38': 0xC0, # CNP - 3 x 8
    'DC_CNP816': 0xC8, # CNP - 8 and 16
    'DC_CNP316': 0xD0, # CNP - 3 x 16
    'DC_CNP232': 0xD8, # CNP - 8, 16, and 2 x 32
    'DC_D32': 0xE0, # Digitizer - 8, 16, and 32
    'DC_COMP': 0xE8, # Digitizer - compression map and multiple samples
    'DC_MULT': 0xF0, # Digitizer - continuation of above
    'DC_SPEC': 0xF8, # Special purpose packets
    'DC_DPSTAT': 0x7F, # DP Statistics
    # DC_MULT flags
    'DMLS': 0x8000, # Last segment
    'DMSZ': 0x3FF, # blockette size mask
    # Reporting Frequencies
    'FR_1': 1, # 1hz
    'FR_P1': 255, # 0.1Hz
    'FR_P01': 254, # 0.01Hz
    # Data Port Records
    'DT_DATA': 0, # Data Record
    'DT_FILL': 6, # Fill Record
    'DT_DACK': 0xA, # Data Acknowledge
    'DT_OPEN': 0xB, # Open firewall and tell DP where to send data
    # GPS Status values
    'GPS_OFFLOCK': 0, # Powered off due to GPS lock
    'GPS_OFFPLL': 1, # Powered off due to PLL lock
    'GPS_OFFMAX': 2, # Powered off due to maximum time
    'GPS_OFFCMD': 3, # Powered off due to command
    'GPS_COLD': 4, # Coldstart, see parameter 2 for reason
    'GPS_ONAUTO': 5, # Powered on automatically
    'GPS_ONCMD': 6, # Powered on by command
    # Group Status Bits
    'GRP_ENON': 1, # Calibration enable on
    'GRP_SGON': 2, # Calibration signal on
    # Status Reporting
    'SRINT_M': 3, # Mask
    'SRINT_OFF': 0, # do not report
    'SRINT_1': 1, # Report at 1Hz
    'SR_1S': 0, # Shift count for status 1
    'SR_2S': 2, # Shift count for status 2
    # Charger Status
    'CHRG_NOT': 0, # Not Charging
    'CHRG_BULK': 1, # Bulk
    'CHRG_ABS': 2, # Absorption
    # Status Messages
    'ST38_GPSPH': 0, # GPS Status Change
    'ST38_CNPERR': 1, # CNP Error
    'ST38_SLVERR': 2, # Slave Processor Error
    'ST38_DIGPH': 3, # Digitizer Phase Change
    'ST38_BACKUP': 4, # Backup configuration
    'ST38_WINSTAT': 5, # Recording Window Status
    'ST38_LEAP': 6, # Leap Second detected
    'ST816_PSPH': 0, # Power Supply Phase Change
    'ST816_AFAULT': 1, # Analog Fault
    'ST816_CALERR': 2, # Calibration Error map
    # ST816_TOKEN = 1 ;    , # Tokens have changed
    'ST32_PLLDRIFT': 0, # PLL Drift over last 10 minutes
    'ST32_DRIFT': 1, # Drift out of range
    # Special Purpose Blockettes
    'SP_CALSTART': 0, # calibration start
    'SP_CALABORT': 1, # calibration abort
    'SP_CNP': 2, # CNP Block data
    'SP_CFGBLK': 3, # Configuration Blockette
    'SP_ENVPROC': 4, # Environmental Processor
    # Digitizer Phase Change Constants, first parameter
    'DPC_STARTUP': 0,
    'DPC_WAIT': 1,
    'DPC_INT': 2,
    'DPC_EXT': 3,
    # Digitizer Phase Change COnstants, second parameter
    'DPR_NORM': 0,
    'DPR_DRIFT': 1,
    'DPR_GOTCLK': 2,
    'DPR_CMD': 3,
    # GPS Coldstart Reasons, second parameter
    'COLD_CMD': 0, # Commanded
    'COLD_TO': 1, # Tracking Timeout
    'COLD_INTPH': 2, # Phasing between GPS and internal clock
    'COLD_JUMP': 3, # Too large a jump while running
    # Recording Window status, first parameter
    'RWR_START': 0,
    'RWR_STOP': 1,
    # Configuration Indexes
    'CFG_GLOBAL': 1, # Global programming
    'CFG_FIXED': 2 , # Fixed
    'CFG_AUXAD': 3 , # AuxAD configuration
    'CFG_SS1': 4, # Serial Sensor 1 configuration
    'CFG_SS2': 5, # Serial Sensor 2 configuration
    'CFGSZ_AUXAD': 20 # Size in bytes of AUXAD configuration
})

# QDP commands :
COMMAND = AttributeDict({
    'C1_CACK':   0xA0,  # Command Acknowledge
    'C1_RQSRV':  0x10,  # Request Server Registration
    'C1_SRVCH':  0xA1,  # Server Challenge
    'C1_SRVRSP': 0x11,  # Server Response
    'C1_CERR':   0xA2,  # Command Error
    'C1_DSRV':   0x12,  # Delete Server
    'C1_POLLSN': 0x14,  # Poll for Serial Number
    'C1_MYSN':   0xA3,  # My Serial Number (response to C1_POLLSN)
    'C1_RQSTAT': 0x1F,  # Request Status
    'C1_RQPHY':  0x16,  # Request Physical Interfaces
    'C1_PHY':    0xA4,  # Physical Interfaces (response to C1_RQPHY)
    'C1_PING':   0x38,  # Ping Q330
    'C1_LOG':    0xA5,  # Data port information
    'C1_RQLOG':  0x18,  # Request data port information
    'C1_DSRV':   0x12,  # Delete server
    'C1_RQFIX':  0x1C,  # Request fixed value after reboot
    'C1_FIX':    0xA7,  # Fixed values after reboot
    'C1_RQMEM':  0x41,  # Request memory content
    'C1_MEM':    0xB8   # Memory content
})

# Ping types :
PING_TYPE = AttributeDict({
    'REQ':         0,
    'RESP':        1,
    'STATUS_REQ':  2,
    'STATUS_RESP': 3,
    'INFO_REQ':    4,
    'INFO_RESP':   5
})

# Status bitmap :
STATUS_BITMAP = AttributeDict({
    'GLOBAL': 0b000000000000000000001,  # 1
    'GPS':    0b000000000000000000010,  # 2
    'PWR':    0b000000000000000000100,  # 4
    'BOOM':   0b000000000000000001000,  # 8
    'THREAD': 0b000000000000000010000,  # 16
    'PLL':    0b000000000000000100000,  # 32
    'SAT':    0b000000000000001000000,  # 64
    'ARP':    0b000000000000010000000,  # 128
    'DP1':    0b000000000000100000000,  # 256
    'DP2':    0b000000000001000000000,  # 512
    'DP3':    0b000000000010000000000,  # 1024
    'DP4':    0b000000000100000000000,  # 2048
    'S1':     0b000000001000000000000,  # 4096
    'S2':     0b000000010000000000000,  # 8192
    'S3':     0b000000100000000000000,  # 16384
    'ETH':    0b000001000000000000000,  # 32768
    'BALER':  0b000010000000000000000,  # 65536
    'DYN_IP': 0b000100000000000000000,  # 131072
    'AUX':    0b001000000000000000000,  # 262144
    'S_SENS': 0b010000000000000000000,  # 524288
    'ENV':    0b100000000000000000000  # 1048576
})

PLL_STATE = AttributeDict({
    'PLL_OFF': 0x00,
    'PLL_HOLD': 0x40,
    'PLL_TRACK': 0x80,
    'PLL_LOCK': 0xC0
})

CLOCK_QUALITY_FLAG = AttributeDict({
    'CQ_LOCK': 1,
    'CQ_2D': 2,
    'CQ_3D': 4,
    'CQ_1D': 8,
    'CQ_FILT': 0x10,
    'CQ_SPEC': 0x20
})

# Error code mapping :
ERROR_CODE = AttributeDict({
    0: "No Permission – Invalid Password.",
    1: "Too Many Configuration or Special Functions Servers registered, cannot add you.",
    2: "You are not registered, please register.",
    3: "Invalid Registration Response, cannot honor your request.",
    4: "Parameter Error.",
    5: "Structure Not Valid, Tried to read an EEPROM structure that is not valid.",
    6: "Configuration Only, you are not allowed to use this COMMAND except on the configuration port.",
    7: "Special Functions Port Only, you are not allowed to use this COMMAND except on the special functions port.",
    8: "Memory Operation in Progress, try your memory access COMMAND again later.",
    9: "Calibration in Progress, try your calibration start or mass re-centering COMMAND later.",
    10: "Data not yet available for QuickView.",
    11: "Console (virtual) Interface Only, you are not allowed to use this COMMAND except on the front panel connector.",
    12: "Flash Write or Erase Error."
})
