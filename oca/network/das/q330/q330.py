#!/usr/bin/python
# -*- coding: utf-8 -*-
from oca.network.das.q330 import constant
from datetime import datetime
import hashlib
import random
import socket
import struct
import json
import sys


class Q330NetworkProtocol(object):

    def __init__(self, address, port=5331, verbose=False, max_attempt=3,
                 timeout=5, serial_number=None, authentication_code=0):
        self.__address = address
        self.__port = port
        self.__verbose = verbose
        self.__crctable = None
        self.__gen_crc_table()
        self.__max_attempt = max_attempt
        self.__timeout = timeout
        self.__authentication_code = int(authentication_code)
        self.__is_authenticated = False
        self.__seq_num = 0
        self.__sock = None
        self.__serial_number = serial_number

    def __gen_crc_table(self):
        '''
        Generate an array of 256 integers used to
        compute the CRC in the function gcrccalc().
        '''
        if self.__crctable:
            return
        self.__crctable = []
        for count in range(0, 256):
            tdata = count << 24
            accum = 0
            for not_used in range(0, 8):
                # Read only the sign bit at 0x80000000.
                if (tdata ^ accum) & 0x80000000:
                    accum = (accum << 1) ^ constant.CRC_POLYNOMIAL
                else:
                    accum = accum << 1
                # Use the mask 0xffffffff to limit the size to 32 bits.
                accum = accum & 0xffffffff
                tdata = (tdata << 1) & 0xffffffff
            self.__crctable.append(accum)

    def __get_crc(self, packet):
        '''
        Compute the CRC from a given packet using the
        given CRC table.

        packet   -- The raw packet as a string.

        return   -- The computed CRC value as an int.
        '''
        crc = 0
        for current_byte in packet:
            temp = ((crc >> 24) ^ ord(current_byte))
            # Use the mask 0xffffffff to limit the size to 32 bits.
            crc = ((crc << 8) ^ self.__crctable[temp]) & 0xffffffff
        return crc

    def __make_qdp_packet(self, command, data=''):
        '''
        Make a QDP packet for the specified command and
        add the given data.

        return -- The QDP packet as a string.
        '''
        qdp_header = constant.STRUCTURE.QDP_HEADER.pack(0, command,
                                                        constant.QDP_VERSION,
                                                        len(data), self.__seq_num, 0)
        self.__seq_num += 1
        qdp_packet = '%s%s' % (qdp_header, data)
        crc = struct.pack('>I', self.__get_crc(qdp_packet[4:]))
        return '%s%s' % (crc, qdp_packet[4:])

    def __make_ping(self, ping_type_value, data=0):
        '''
        Make a PING QDP packet with the given type
        and the given data. The data must not exceed
        32 bits (4 bytes).

        return -- The PING QDP packet as a string.
        '''
        if ping_type_value == constant.PING_TYPE.REQ:
            ping_data = struct.pack('>HHI', ping_type_value, 0, data)
        elif ping_type_value == constant.PING_TYPE.STATUS_REQ:
            ping_data = struct.pack('>HHI', ping_type_value, 0, data)
        elif ping_type_value == constant.PING_TYPE.INFO_REQ:
            ping_data = struct.pack('>HH', ping_type_value, 0)
        return self.__make_qdp_packet(constant.COMMAND.C1_PING, ping_data)

    def __authenticate(self):
        if self.__is_authenticated:
            return
        hex_characters = '0123456789abcdef'
        server_random_value = ''.join([random.choice(hex_characters) for x in range(0, 16)])
        if self.__serial_number is None:
            info_ping_result = self.send_ping(ping_type=constant.PING_TYPE.INFO_REQ)
            if info_ping_result['data']['serial_number'] == 0:
                raise ValueError('Interface is locked, serial number can\'t be retrieved : authentication aborted.')
            self.__serial_number = info_ping_result['data']['serial_number']
        self.__seq_num = 0

        # STEP 1: send C1_RQSRV
        rqsrv_data = constant.STRUCTURE.C1_RQSRV.pack(int(self.__serial_number, 16))
        rqsrv_packet = self.__make_qdp_packet(constant.COMMAND.C1_RQSRV, rqsrv_data)

        # STEP 2: receive C1_SRVCH
        srvch_result = self.__send_packet(rqsrv_packet)
        if srvch_result['command'] == constant.COMMAND.C1_CERR:
            raise ValueError(srvch_result['data']['message'])
        elif srvch_result['command'] != constant.COMMAND.C1_SRVCH:
            raise ValueError('Unexpected response (command=0x%02x)' % srvch_result['command'])

        # Prepare C1_SRVRSP
        challenge_response = ''.join([
            '%016x' % srvch_result['data']['challenge_value'],
            '%08x' % srvch_result['data']['ip_address'],
            '%04x' % srvch_result['data']['udp_port'],
            '%04x' % srvch_result['data']['registration_number'],
            '%016x' % self.__authentication_code,
            '%016x' % int(self.__serial_number, 16),
            server_random_value
        ])
        md5_hash = hashlib.md5(challenge_response)
        md5_int = int(md5_hash.hexdigest(), 16)
        c1_srvrsp_data = constant.STRUCTURE.C1_SRVRSP.pack(
            int(self.__serial_number, 16),
            srvch_result['data']['challenge_value'],
            srvch_result['data']['ip_address'],
            srvch_result['data']['udp_port'],
            srvch_result['data']['registration_number'],
            int(server_random_value, 16),
            md5_int >> 64,
            md5_int & 0xffffffffffffffff
        )
        c1_srvrsp_packet = self.__make_qdp_packet(constant.COMMAND.C1_SRVRSP, c1_srvrsp_data)
        response = self.__send_packet(c1_srvrsp_packet)
        if response['command'] == constant.COMMAND.C1_CERR:
            raise ValueError(response['data']['message'])
        elif response['command'] == constant.COMMAND.C1_CACK:
            self.__is_authenticated = True
        else:
            raise ValueError('Unexpected response (command=0x%02x)' % response['command'])

    def __parse_header(self, packet):
        '''
        Decode the header of a QDP packet.
        '''
        header_key = ('crc', 'command', 'version', 'length',
                      'seq_num', 'ack_num')
        header_val = constant.STRUCTURE.QDP_HEADER.unpack(packet[:constant.STRUCTURE.QDP_HEADER.size])
        result = dict(zip(header_key, header_val))
        my_crc = self.__get_crc(packet[4:])
        if result['crc'] != my_crc:
            raise ValueError('Error: The CRC is not valid : %x instead of %x.\n' %
                             (result['crc'], my_crc))
        return result

    def __parse_global_status(self, data):
        """
        """
        global_key = ('acq_ctrl', 'clock_quality', 'clock_loss',
                      'current_voltage', 'sec_offset', 'usec_offset',
                      'total_time', 'power_time', 'last_resync',
                      'gps_status', 'cal_status', 'sensor_map',
                      'current_vco', 'data_seq', 'pll_flag', 'status_inputs',
                      'misc_inputs', 'cur_sequence')
        packet_size = constant.STRUCTURE.STATUS_GLOBAL.size
        gloval_val = constant.STRUCTURE.STATUS_GLOBAL.unpack(data[:packet_size])
        status_result = dict(zip(global_key, gloval_val))
        data_seq = status_result.pop('data_seq')
        seconds = status_result.pop('sec_offset')
        microseconds = status_result.pop('usec_offset')
        #  status_result['offset'] = constant.EPOCH + seconds + microseconds * pow(10, -6)
        status_result['offset'] = data_seq + seconds + microseconds * pow(10, -6)
        status_result['last_resync'] += constant.EPOCH
        return status_result, packet_size

    def __parse_gps_status(self, data):
        gps_key = ('gps_time', 'gps_on', 'sat_used', 'sat_view',
                   'time', 'date', 'fix', 'altitude', 'latitude',
                   'longitude', 'last_good', 'check_err')
        packet_size = constant.STRUCTURE.STATUS_GPS.size
        gps_val = constant.STRUCTURE.STATUS_GPS.unpack(data[:packet_size])
        status_result = dict(zip(gps_key, gps_val))
        for current_key in gps_key[4:10]:
            str_len = ord(status_result[current_key][0])
            status_result[current_key] = status_result[current_key][1:1 + str_len]
        if status_result['gps_on'] != 0:
            status_result['altitude'] = float(status_result['altitude'][:-1])
            lat = status_result['latitude']
            lon = status_result['longitude']
            status_result['latitude'] = float(lat[:-1]) / 100
            status_result['longitude'] = float(lon[:-1]) / 100
            if lat.endswith('S'):
                status_result['latitude'] *= -1
            if lon.endswith('S'):
                status_result['longitude'] *= -1
            status_result['datetime'] = int(datetime.strptime('%s %s' % (status_result['date'],
                                                                         status_result['time']),
                                                              '%d/%m/%Y %H:%M:%S').strftime('%s'))
        else:
            status_result['altitude'] = None
            status_result['latitude'] = None
            status_result['longitude'] = None
            status_result['datetime'] = None
        status_result['last_good'] += constant.EPOCH
        return status_result, packet_size

    def __parse_pwr_status(self, data):
        smu_key = ('charging_phase', 'batt_temp', 'batt_capacity', 'depth',
                   'batt_voltage', 'input_voltage', 'batt_current',
                   'absorption_sp', 'float_sp', 'alert', 'loads_off')
        packet_size = constant.STRUCTURE.STATUS_PWR.size
        smu_val = constant.STRUCTURE.STATUS_PWR.unpack(data[:packet_size])
        status_result = dict(zip(smu_key, smu_val))

        return status_result, packet_size

    def __parse_boom_status(self, data):
        boom_key = ('boom1', 'boom2', 'boom3', 'boom4', 'boom5', 'boom6',
                    'amn_pos', 'amb_neg', 'supply', 'sys_temp',
                    'main_current', 'ant_current', 'seis1_temp',
                    'seis2_temp', 'timeouts')
        packet_size = constant.STRUCTURE.STATUS_BOOM.size
        boom_val = constant.STRUCTURE.STATUS_BOOM.unpack(data[:packet_size])
        status_result = dict(zip(boom_key, boom_val))
        if status_result['seis1_temp'] == 666:
            status_result['seis1_temp'] = None
        if status_result['seis2_temp'] == 666:
            status_result['seis2_temp'] = None
        status_result['amn_pos'] = status_result['amn_pos'] / 100.
        status_result['supply'] = (status_result['supply'] * 150) / 1000.

        return status_result, packet_size

    def __parse_pll_status(self, data):
        pll_key = ('initial_vco', 'time_error', 'rms_vco', 'best_vco',
                   'spare', 'ticks_track_lock', 'km', 'state')
        packet_size = constant.STRUCTURE.STATUS_PLL.size
        pll_val = constant.STRUCTURE.STATUS_PLL.unpack(data[:packet_size])
        status_result = dict(zip(pll_key, pll_val))
        status_result.pop('spare')

        return status_result, packet_size

    def __parse_sat_status(self, data):
        header_end = constant.STRUCTURE.STATUS_SAT1.size
        nb_entries, packet_size = constant.STRUCTURE.STATUS_SAT1.unpack(data[:header_end])
        sat_key = ('id', 'elevation', 'azimuth', 'snr')
        status_result = {'sat': []}
        for sat_index in range(0, nb_entries):
            begin = (constant.STRUCTURE.STATUS_SAT1.size
                     + sat_index * constant.STRUCTURE.STATUS_SAT2.size)
            end = begin + constant.STRUCTURE.STATUS_SAT2.size
            current_sat_val = constant.STRUCTURE.STATUS_SAT2.unpack(data[begin:end])
            status_result['sat'].append(dict(zip(sat_key, current_sat_val)))

        return status_result, packet_size

    def __parse_arp_status(self, data):
        header_end = constant.STRUCTURE.STATUS_ARP1.size
        nb_entries, packet_size = constant.STRUCTURE.STATUS_ARP1.unpack(data[:header_end])
        status_result = {'arp': []}
        for index in range(0, nb_entries):
            begin = (constant.STRUCTURE.STATUS_ARP1.size
                     + index * constant.STRUCTURE.STATUS_ARP2.size)
            end = begin + constant.STRUCTURE.STATUS_ARP2.size
            current_arp_val = constant.STRUCTURE.STATUS_ARP2.unpack(data[begin:end])
            current_status = {}
            current_status['ip'] = '%s.%s.%s.%s' % current_arp_val[0:4]
            current_status['mac'] = ('%02x:%02x:%02x:%02x:%02x:%02x' %
                                     current_arp_val[4:10])
            current_status['timeout'] = current_arp_val[10]
            status_result['arp'].append(current_status)

        return status_result, packet_size

    def __parse_thread_status(self, data):
        thread1_key = ('nb_entries', 'length', 'sys_time_h', 'sys_time_l',
                       'spare')
        thread2_key = ('run_time_h', 'run_time_l', 'priority',
                       'priority_count', 'last_run_h', 'last_run_l', 'flags')

        header_end = constant.STRUCTURE.STATUS_THREAD1.size
        thread1_val = constant.STRUCTURE.STATUS_THREAD1.unpack(data[:header_end])
        status_result = dict(zip(thread1_key, thread1_val))
        packet_size = status_result.pop('length')
        status_result['sys_time'] = (status_result.pop('sys_time_h') << 16
                                     | status_result.pop('sys_time_l'))
        status_result.pop('spare')
        status_result['thread'] = []
        for i in range(0, status_result['nb_entries']):
            begin = (constant.STRUCTURE.STATUS_THREAD1.size
                     + i * constant.STRUCTURE.STATUS_THREAD2.size)
            end = begin + constant.STRUCTURE.STATUS_THREAD2.size
            current_thread_val = constant.STRUCTURE.STATUS_THREAD2.unpack(data[begin:end])
            current_status = dict(zip(thread2_key, current_thread_val))
            current_status['run_time'] = (current_status.pop('run_time_h') << 16
                                          | current_status.pop('run_time_l'))
            current_status['last_run'] = (current_status.pop('last_run_h') << 16
                                          | current_status.pop('last_run_l'))
            status_result['thread'].append(current_status)

        return status_result, packet_size

    def __parse_status_data(self, data):
        '''
        Parse a status data structure (data without QDP header
        but the status bitmap must be given) and return an object
        with all available information from the given data.

        return -- Information extract from data as a list of dictionary.
                  Each dictionary own the key 'status_bitmap' that can be
                  used to determine which kind of status data it gives.
        '''
        offset = 4
        bitmap_value = struct.unpack('>I', data[:offset])[0]
        bitmap_list = filter(lambda x: (bitmap_value & x) > 0,
                             constant.STATUS_BITMAP.values())
        bitmap_list.sort()
        status_data = []
        data = data[4:]
        for bitmap in bitmap_list:

            # Global status
            if bitmap == constant.STATUS_BITMAP.GLOBAL:
                status_result, packet_length = self.__parse_global_status(data)

            # GPS status
            elif bitmap == constant.STATUS_BITMAP.GPS:
                status_result, packet_length = self.__parse_gps_status(data)

            # Power supply status
            elif bitmap == constant.STATUS_BITMAP.PWR:
                status_result, packet_length = self.__parse_pwr_status(data)

            # Boom positions, temperatures, and voltages
            elif bitmap == constant.STATUS_BITMAP.BOOM:
                status_result, packet_length = self.__parse_boom_status(data)

            # PLL status
            elif bitmap == constant.STATUS_BITMAP.PLL:
                status_result, packet_length = self.__parse_pll_status(data)

            # GPS satellites
            elif bitmap == constant.STATUS_BITMAP.SAT:
                status_result, packet_length = self.__parse_sat_status(data)
            # ARP
            elif bitmap == constant.STATUS_BITMAP.ARP:
                status_result, packet_length = self.__parse_arp_status(data)

            # Thread
            elif bitmap == constant.STATUS_BITMAP.THREAD:
                status_result, packet_length = self.__parse_thread_status(data)

            # All unhandled status data
            else:
                raise ValueError("Can't handle bitmap %s" % bitmap)

            status_result['status_bitmap'] = bitmap
            status_data.append(status_result)
            data = data[packet_length:]

        return status_data

    def _parse(self, packet):
        header = self.__parse_header(packet)
        data = packet[constant.STRUCTURE.QDP_HEADER.size:]
        result = header
        result['data'] = {}

        # C1_CERR
        if header['command'] == constant.COMMAND.C1_CERR:
            result['data'] = {'errno': None, 'message': 'No error message'}
            if header['length']:
                errno = struct.unpack('>H', data)[0]
                result['data'] = {'errno': errno,
                                  'message': constant.ERROR_CODE[errno]}
            return result

        # C1_SRVCH
        elif header['command'] == constant.COMMAND.C1_SRVCH:
            c1_srvch_key = ('challenge_value', 'ip_address', 'udp_port', 'registration_number')
            c1_srvch_val = constant.STRUCTURE.C1_SRVCH.unpack(data)
            result['data'] = dict(zip(c1_srvch_key, c1_srvch_val))
            return result

        # C1_CACK
        elif header['command'] == constant.COMMAND.C1_CACK:
            result['data'] = dict()
            return result

        # C1_FIX
        elif header['command'] == constant.COMMAND.C1_FIX:
            if constant.STRUCTURE.C1_FIX.size != len(data):
                raise ValueError('not matching data length %s instead of %s' %
                                 (constant.STRUCTURE.C1_FIX.size, len(data)))
            c1_fix_key = ('last_reboot_time', 'nb_reboot', 'backup_data_structure',
                          'default_data_structure', 'calibrator_type', 'calibrator_version',
                          'aux_type', 'aux_version', 'clock_type', 'flags', 'system_version',
                          'slave_processor_version', 'pld_version', 'memory_block_size',
                          'kmi_property_tag', 'system_sn', 'analog_mother_board_sn',
                          'sensor_1_sn', 'sensor_2_sn', 'qapchp_1_sn', 'internal_data_mem_size',
                          'internal_data_mem_used', 'external_data_mem_size', 'flash_mem_size',
                          'external_data_mem_used', 'qapchp_2_sn', 'dp_1_packet_mem_size',
                          'dp_2_packet_mem_size', 'dp_3_packet_mem_size', 'dp_4_packet_mem_size',
                          'freq_7', 'freq_6', 'freq_5', 'freq_4', 'freq_3', 'freq_2', 'freq_1', 'freq_0',
                          'channel_1-3_bit_7_freq_delay', 'channel_1-3_bit_6_freq_delay',
                          'channel_1-3_bit_5_freq_delay', 'channel_1-3_bit_4_freq_delay',
                          'channel_1-3_bit_3_freq_delay', 'channel_1-3_bit_2_freq_delay',
                          'channel_1-3_bit_1_freq_delay', 'channel_1-3_bit_0_freq_delay',
                          'channel_4-6_bit_7_freq_delay', 'channel_4-6_bit_6_freq_delay',
                          'channel_4-6_bit_5_freq_delay', 'channel_4-6_bit_4_freq_delay',
                          'channel_4-6_bit_3_freq_delay', 'channel_4-6_bit_2_freq_delay',
                          'channel_4-6_bit_1_freq_delay', 'channel_4-6_bit_0_freq_delay')
            c1_fix_val = constant.STRUCTURE.C1_FIX.unpack(data)
            result['data'] = dict(zip(c1_fix_key, c1_fix_val))
            return result

        # C1_LOG
        elif header['command'] == constant.COMMAND.C1_LOG:
            c1_log_key = ('data_port_number', 'flags', 'percent_packet_buffer', 'mtu',
                          'group_count', 'max_resend_timeout', 'group_timeout',
                          'minimum_resend_timeout', 'window_size', 'data_sequence',
                          'channel_1_freq', 'channel_2_freq', 'channel_3_freq',
                          'channel_4_freq', 'channel_5_freq', 'channel_6_freq',
                          'acknowledge_count', 'acknowledge_timeout',
                          'old_data_threshold_time', 'ethernet_throttle',
                          'alarm_percent', 'automatic_filters', 'manual_filters', 'spare')
            c1_log_val = constant.STRUCTURE.C1_LOG.unpack(data)
            result['data'] = dict(zip(c1_log_key, c1_log_val))
            return result

        # C1_MEM
        elif header['command'] == constant.COMMAND.C1_MEM:
            c1_mem_key = ('starting_address', 'byte_count', 'memory_type', 'seg_num', 'max_seg')
            c1_mem_val = constant.STRUCTURE.C1_MEM.unpack(data[:constant.STRUCTURE.C1_MEM.size])
            result['data'] = dict(zip(c1_mem_key, c1_mem_val))
            result['data']['memory_content'] = data[constant.STRUCTURE.C1_MEM.size:]
            return result

        # C1_PHY
        elif header['command'] == constant.COMMAND.C1_PHY:
            c1_phy_key = ('serial_number',
                          'IP_serial_interface_1', 'IP_serial_interface_2', 'IP_serial_interface_3',
                          'IP_ethernet', 'ethernet_mac_high', 'ethernet_mac_low', 'base_port',
                          'baud_serial_interface_1', 'flags_serial_interface_1',
                          'baud_serial_interface_2', 'flags_serial_interface_2',
                          'baud_serial_interface_3', 'flags_serial_interface_3',
                          'reserved', 'flags_ethernet',
                          'throttle_serial_interface 1',
                          'throttle_serial_interface 2',
                          'throttle_serial_interface 3',
                          'reserved')
            c1_phy_val = constant.STRUCTURE.C1_PHY.unpack(data)
            result['data'] = dict(zip(c1_phy_key, c1_phy_val))
            result['data']['serial_number'] = '%016x' % result['data']['serial_number']
            return result

        # C1_MYSN
        elif header['command'] == constant.COMMAND.C1_MYSN:
            c1_mysn_key = ('serial_number', 'kmi_tag', 'user_tag')
            c1_mysn_val = constant.STRUCTURE.C1_MYSN.unpack(data)
            result['data'] = dict(zip(c1_mysn_key, c1_mysn_val))
            result['data']['serial_number'] = '%016x' % result['data']['serial_number']
            return result

        # C1_PING
        elif header['command'] == constant.COMMAND.C1_PING:
            ping_type_value = struct.unpack('>H', data[:2])[0]

            # PING request
            if ping_type_value == constant.PING_TYPE.REQ:
                result['data']['ping_type'] = 0

            # PING response
            elif ping_type_value == constant.PING_TYPE.RESP:
                result['data']['ping_type'] = 1

            # Status request
            elif ping_type_value == constant.PING_TYPE.STATUS_REQ:
                status_req_key = ('ping_type', 'user_msg_num', 'status_bitmap')
                status_req_val = constant.STRUCTURE.PING_STATUS_REQ.unpack(data)
                result['data'] = dict(zip(status_req_key, status_req_val))

            # Status response
            elif ping_type_value == constant.PING_TYPE.STATUS_RESP:
                now = int(datetime.utcnow().strftime('%s'))
                status_resp_key = ('ping_type', 'user_msg_num', 'drift_tolerance',
                                   'user_msg_count', 'last_reboot', 'spare',
                                   'spare', 'status_bitmap')
                end = constant.STRUCTURE.PING_STATUS_RESP.size
                status_resp_val = constant.STRUCTURE.PING_STATUS_RESP.unpack(data[:end])
                result['data'] = dict(zip(status_resp_key, status_resp_val))
                result['data'].pop('spare')
                result['data']['uptime'] = now - (constant.EPOCH + result['data']['last_reboot'])
                begin = constant.STRUCTURE.PING_STATUS_RESP.size - 4
                result['data']['status'] = self.__parse_status_data(data[begin:])

            # Information request
            elif ping_type_value == constant.PING_TYPE.INFO_REQ:
                result['data']['ping_type'] = 4

            # Information response
            elif ping_type_value == constant.PING_TYPE.INFO_RESP:
                info_key = ('ping_type', 'ignored', 'version', 'flags', 'kmi_tag',
                            'serial_number', 'dp1_mem', 'dp2_mem', 'dp3_mem',
                            'dp4_mem', 'serial1_mem_trig', 'serial2_mem_trig',
                            'reserved', 'eth_mem_trig', 'serial1_flags',
                            'serial2_flags', 'reserved', 'eth_flags',
                            'serial1_dp_num', 'serial2_dp_num', 'reserved',
                            'eth_dp_num', 'cal_err_bitmap', 'sys_software_version')
                end = constant.STRUCTURE.PING_INFO_RESP.size
                info_val = constant.STRUCTURE.PING_INFO_RESP.unpack(data[:end])
                result['data'] = dict(zip(info_key, info_val))
                result['data']['serial_number'] = '%016x' % result['data']['serial_number']
                result['data'].pop('reserved')

            # Unknown PING type
            else:
                raise ValueError('Unknown PING type %s' % ping_type_value)
            return result

        else:
            raise ValueError('Unhandled packet command=0x%02x' %
                             header['command'])
            return None

    def __parse_config(self, mem):
        fixed = self.get_fixed_values_after_reboot()

        config = {}

        i = 0

        def load(s, i):
            val = s.unpack(mem[i:i+s.size])
            return val[0], i + s.size

        while i < len(mem):
            tok, i = load(constant.TYPE.BYTE, i)

            if tok == constant.CONFIG_TOKEN.TF_NOP:
                pass

            elif tok == constant.CONFIG_TOKEN.TF_NET_STAT:
                config['network'] = mem[i:i+2].strip()
                config['station'] = mem[i+2:i+7].strip()
                i += 7

            elif tok == constant.CONFIG_TOKEN.TF_NETSERV:
                config['netport'], i = load(constant.TYPE.WORD, i)

            elif tok == constant.CONFIG_TOKEN.TF_DSS:
                dss_key = ('high_pass', 'mid_pass', 'low_pass', 'timeout',
                           'max_bps', 'verbosity', 'max_cpu_perc', 'port_number',
                           'max_mem', 'reserved')
                dss_val = constant.STRUCTURE.TF_DSS.unpack(mem[i:i+constant.STRUCTURE.TF_DSS.size])
                config['dss'] = dict(zip(dss_key, dss_val))
                i += constant.STRUCTURE.TF_DSS.size

            elif tok == constant.CONFIG_TOKEN.TF_WEB:
                config['webport'], i = load(constant.TYPE.WORD, i)

            elif tok == constant.CONFIG_TOKEN.TF_DATASERV:
                config['dservport'], i = load(constant.TYPE.WORD, i)

            elif tok == constant.CONFIG_TOKEN.TF_CLOCK:
                clock_key = ('zone', 'degrade_time', 'locked', 'track', 'hold'
                             'off', 'spare', 'high', 'low', 'never', 'clock_filt')
                clock_val = constant.STRUCTURE.TF_CLOCK.unpack(mem[i:i+constant.STRUCTURE.TF_CLOCK.size])
                config['clock'] = dict(zip(clock_key, clock_val))
                i += constant.STRUCTURE.TF_CLOCK.size

            elif tok == constant.CONFIG_TOKEN.TF_MT:
                mt_key = ('log_location', 'log_seedname', 'time_location', 'time_seedname')
                mt_val = constant.STRUCTURE.TF_MT.unpack(mem[i:i+constant.STRUCTURE.TF_MT.size])
                config['log'] = dict(zip(mt_key, mt_val))
                i += constant.STRUCTURE.TF_MT.size

            elif tok == constant.CONFIG_TOKEN.TF_CFG:
                cfg_key = ('cfg_location', 'cfg_seedname', 'flags', 'interval')
                cfg_val = constant.STRUCTURE.TF_CFG.unpack(mem[i:i+constant.STRUCTURE.TF_CFG.size])
                config['cfg'] = dict(zip(cfg_key, cfg_val))
                i += constant.STRUCTURE.TF_CFG.size

            elif tok in [constant.CONFIG_TOKEN.T1_MHD, constant.CONFIG_TOKEN.T1_THRD]:
                pref = i
                next, i = load(constant.TYPE.BYTE, i)
                # skip decoding detector
                i = pref + next

            elif tok == constant.CONFIG_TOKEN.T1_IIR:
                pref = i
                next, i = load(constant.TYPE.BYTE, i)
                # skip decoding IIR
                i = pref + next

            elif tok == constant.CONFIG_TOKEN.T1_LCQ:
                pref = i
                next, i = load(constant.TYPE.BYTE, i)
                lcq_key = ('location', 'seedname', 'lcq_num', 'raw_data_source',
                           'raw_data_field', 'lcq_opt', 'rate')
                lcq_val = constant.STRUCTURE.T1_LCQ.unpack(mem[i:i+constant.STRUCTURE.T1_LCQ.size])
                cur_lcq = dict(zip(lcq_key, lcq_val))
                i += constant.STRUCTURE.T1_LCQ.size
                if (cur_lcq['raw_data_source'] & constant.DATA_CH.DCM) == constant.DATA_CH.DC_D32:
                    cur_lcq['physical_channel'] = cur_lcq['raw_data_source'] & 0x0f
                    if cur_lcq['raw_data_field'] == 7:
                        real_rate = None
                        freq7 = fixed['feq_7'] & 0x7f
                        if freq7 == 5:
                            real_rate = 250
                        elif freq7 == 8:
                            real_rate = 500
                        elif freq7 == 10:
                            real_rate = 1000
                        else:
                            real_rate = 0
                        cur_lcq['rate'] = real_rate
                else:
                    # keep only sismo channels ignore others
                    i = pref + next
                    continue
                if cur_lcq['lcq_opt'] & constant.LCQ_OPT.LO_PEB:
                    cur_lcq['pre_event_buffers'], i = load(constant.TYPE.WORD, i)
                if cur_lcq['lcq_opt'] & constant.LCQ_OPT.LO_GAP:
                    cur_lcq['gap_threshold'], i = load(constant.TYPE.WORD, i)  # TODO: handle type 'single'
                    if cur_lcq['gap_threshold'] == 0.0:
                        cur_lcq['gap_threshold'] = 0.5
                if cur_lcq['lcq_opt'] & constant.LCQ_OPT.LO_CALDLY:
                    cur_lcq['caldly'], i = load(constant.TYPE.WORD, i)
                if cur_lcq['lcq_opt'] & constant.LCQ_OPT.LO_FRAME:
                    cur_lcq['maxframes'], i = load(constant.TYPE.BYTE, i)
                if cur_lcq['lcq_opt'] & constant.LCQ_OPT.LO_FIRMULT:
                    cur_lcq['firfixing_gain'], i = load(constant.TYPE.WORD, i)  # TODO: handle type 'single'
                if cur_lcq['lcq_opt'] & constant.LCQ_OPT.LO_AVG:
                    cur_lcq['avg_length'], i = load(constant.TYPE.LONGWORD, i)
                    b, i = load(constant.TYPE.BYTE, i)
                    if b != 0xff:
                        # skip setting avg source
                        pass
                    _, i = load(constant.TYPE.LONGWORD, i)
                    _, i = load(constant.TYPE.BYTE, i)
                if cur_lcq['lcq_opt'] & constant.LCQ_OPT.LO_CDET:
                    b, i = load(constant.TYPE.BYTE, i)
                    cur_lcq['ctrl'] = 0xffffff00 | b
                if cur_lcq['lcq_opt'] & constant.LCQ_OPT.LO_DEC:
                    _, i = load(constant.TYPE.BYTE, i)
                    # skip code here (libtokens.c:352)
                    _, i = load(constant.TYPE.BYTE, i)
                    # skip resolving FIR
                mask = constant.LCQ_OPT.LO_DET1
                while mask <= constant.LCQ_OPT.LO_DET8:
                    if cur_lcq['lcq_opt'] & mask:
                        # skip code here (libtokens.c:385)
                        _, i = load(constant.TYPE.BYTE, i)
                        _, i = load(constant.TYPE.BYTE, i)
                        _, i = load(constant.TYPE.BYTE, i)
                    mask = mask << 1

                if 'lcq' not in config:
                    config['lcq'] = list()
                config['lcq'].append(cur_lcq)
                i = pref + next

            elif tok == constant.CONFIG_TOKEN.T1_DPLCQ:
                pref = i
                next, i = load(constant.TYPE.BYTE, i)
                # TODO: decode DPLCQ
                i = pref + next

            elif tok == constant.CONFIG_TOKEN.T1_CTRL:
                pref = i
                next, i = load(constant.TYPE.BYTE, i)
                # skip decoding control detector
                i = pref + next

            elif tok == constant.CONFIG_TOKEN.T1_NONCOMP:
                pref = i
                next, i = load(constant.TYPE.BYTE, i)
                config['non_comp'] = True
                i = pref + next

            elif tok == constant.CONFIG_TOKEN.T2_CNAMES:
                pref = i
                next, i = load(constant.TYPE.WORD, i)
                # skip decoding com events
                i = pref + next

            elif tok == constant.CONFIG_TOKEN.T2_OPAQUE:
                pref = i
                next, i = load(constant.TYPE.WORD, i)
                # skip decoding com events
                i = pref + next

            else:
                if tok >= 0x80:
                    pref = i
                    if tok >= 0xc0:
                        next, i = load(constant.TYPE.WORD, i)
                    else:
                        next, i = load(constant.TYPE.BYTE, i)
                    i = pref + next
        return config

    def __send_packet(self, packet, ping_data=0):
        '''
        Send the given packet to the remote Q330.

        return -- The reponse to the sent packet as dictionnary.
        '''
        nb_attempt = 0
        if self.__verbose:
            sys.stderr.write('my request packet (%s bytes):\n' % len(packet))
            sys.stderr.write('%s\n\n' % packet.encode('string-escape'))

        if self.__sock is None:
            self.__sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.__sock.settimeout(self.__timeout)

        while nb_attempt < self.__max_attempt:
            try:
                self.__sock.sendto(packet, (self.__address, self.__port))

                data, server = self.__sock.recvfrom(4096)

                if self.__verbose:
                    sys.stderr.write('response packet (%s bytes):\n' % len(data))
                    sys.stderr.write('%s\n\n' % data.encode('string-escape'))

                # sock.close()
                return self._parse(data)

            except socket.timeout:
                if self.__verbose:
                    sys.stderr.write('Attempt %s of %s : got timeout, retry...\n' %
                                     (nb_attempt + 1, self.__max_attempt))
                nb_attempt += 1

        raise socket.error('The remote Q330 seems unreachable (address: %s, port: UDP/%s)' % (self.__address, self.__port))

    def send_ping(self, ping_type, ping_data=0):
        '''
        Send a PING with the given properties.

        ping_type -- The type of the ping.
        ping_data -- Bitmap bitwise (kinemetrics.constant.STATUS_BITMAP)

        return    -- The response of the ping as a dictionary.
        '''
        ping_packet = self.__make_ping(ping_type, ping_data)
        return self.__send_packet(ping_packet)

    def get_data_port(self, port_number):
        self.__authenticate()
        request_packet_data = constant.STRUCTURE.C1_RQLOG.pack(port_number)
        request_packet = self.__make_qdp_packet(constant.COMMAND.C1_RQLOG, request_packet_data)
        return self.__send_packet(request_packet)

    def get_serial_number(self):
        request_packet = self.__make_qdp_packet(constant.COMMAND.C1_POLLSN, '\xff\xff\xff\xff')
        return self.__send_packet(request_packet)

    def get_physical_interfaces_config(self):
        self.__authenticate()
        request_packet = self.__make_qdp_packet(constant.COMMAND.C1_RQPHY)
        return self.__send_packet(request_packet)

    def get_fixed_values_after_reboot(self):
        self.__authenticate()
        request_packet = self.__make_qdp_packet(constant.COMMAND.C1_RQFIX)
        return self.__send_packet(request_packet)

    def get_data_port_config(self, data_port_num):
        if data_port_num not in [1, 2, 3, 4]:
            raise ValueError('Data port number must be between 1 and 4')
        self.__authenticate()
        addr = 0
        cur_seg = 1
        max_seg = 1
        mem_chunk = []
        while cur_seg <= max_seg:
            request_packet_data = constant.STRUCTURE.C1_RQMEM.pack(
                                      addr, 0, data_port_num,
                                      self.__authentication_code >> 64,
                                      self.__authentication_code & 0xffffffff)
            request_packet = self.__make_qdp_packet(constant.COMMAND.C1_RQMEM,
                                                    request_packet_data)
            response = self.__send_packet(request_packet)
            # if response
            max_seg = response['data']['max_seg']
            mem_chunk.append(response['data']['memory_content'][:response['data']['byte_count']])
            addr = response['data']['starting_address'] + response['data']['byte_count'] + 6
            cur_seg += 1
        return self.__parse_config(''.join(mem_chunk))

    def disconnect(self):
        if not self.__is_authenticated:
            return
        dsrv_data = constant.STRUCTURE.C1_DSRV.pack(int(self.__serial_number, 16))
        dsvr_packet = self.__make_qdp_packet(constant.COMMAND.C1_DSRV, dsrv_data)
        result = self.__send_packet(dsvr_packet)
        self.__is_authenticated = False
        if result['command'] == constant.COMMAND.C1_CERR:
            raise ValueError(result['data']['message'])
        elif result['command'] != constant.COMMAND.C1_CACK:
            raise ValueError('Unexpected response (command=0x%02x)' % result['command'])


if __name__ == '__main__':
    # Exemple d'utilisation :

    # definir l'IP et le port :
    address = '80.11.16.51'
    port = 6330

    # address = '80.11.16.51'
    # port = 6330

    # creation d'une instance de la class Q330NetworkProtocol
    myQuanterra = Q330NetworkProtocol(address, port, verbose=False, authentication_code=0)

    # envoi du ping qui demande des infos de status multiples
    # result = myQuanterra.send_ping(ping_type=constant.PING_TYPE.STATUS_REQ,
    #                                ping_data=(constant.STATUS_BITMAP.GLOBAL |
    #                                           constant.STATUS_BITMAP.GPS |
    #                                           constant.STATUS_BITMAP.BOOM |
    #                                           constant.STATUS_BITMAP.SAT))
    # result = myQuanterra.send_ping(ping_type=constant.PING_TYPE.INFO_REQ)
    # result = myQuanterra.get_physical_interfaces_config()

    try:
        # result = myQuanterra.get_fixed_values_after_reboot()
        result = myQuanterra.get_data_port_config(1)
    finally:
        myQuanterra.disconnect()
    # result = myQuanterra.send_ping(ping_type=constant.PING_TYPE.INFO_REQ)

    # affichage du resultat :
    # print(result)
    print(json.dumps(result, indent=2, sort_keys=True))
