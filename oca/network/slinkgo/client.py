#!/usr/bin/python27
# -*- coding: utf-8 -*-
import re
import json
import socket
from datetime import datetime
from collections import defaultdict

DATE_WITHOUT_NANO_RE = '([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{6})'
DATE_WITHOUT_MICRO = '([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z)'


def normalize_date(unormalized_date):
    m = re.match(DATE_WITHOUT_NANO_RE, unormalized_date)
    if m:
        return "%sZ" % m.groups(0)
    m = re.match(DATE_WITHOUT_MICRO, unormalized_date)
    if m:
        return "%s.0Z" % m.groups()[0][:-1]
    return unormalized_date


class SlinkgoClientOld(object):

    """
    Initialize this SlinkgoClient.

    address -- The slinkgo server address
    port    -- The slinkgo server port (default:1234)
    """
    def __init__(self, address, port=1234):
        self.__address = address
        self.__port = port

    def _init_connection(self):
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__sock.settimeout(5)
        self.__sock.connect((self.__address, self.__port))

    def _send_request(self, request):
        sent = 0
        while sent < len(request):
            sent += self.__sock.send(request[sent:])

    def _recv_response(self):
        """
        Read request response

        return -- The request response as string
        """
        data = ''
        dataTmp = self.__sock.recv(4096)
        while dataTmp:
            data += dataTmp
            dataTmp = self.__sock.recv(4096)
        return data

    def get_age(self, stream_list):
        """
        Initiate de age request to slinkgo server
        for a given stream list.

        stream_list -- The list of stream id (NN_SS_LL_CCC)

        return      -- A dictionnary like:
                       {
                         "dataage": {
                           <streamid>: <dataage in seconds>,
                           ...
                         },
                         "packetage": {
                           <streamid>: <packetage in seconds>,
                         }
                       }
        """
        self._init_connection()
        request = json.dumps({"Command": "age", "Parameter": stream_list})
        self._send_request(request)
        response = self._recv_response()
        obj = json.loads(response)
        return obj

    def get_stat(self):
        """
        Initiate an STAT request to slinkgo server

        return -- A dictionary.
        """
        self._init_connection()
        request = json.dumps({"Command": "stat"})
        self._send_request(request)
        response = self._recv_response()
        obj = json.loads(response)
        return obj

    def get_gaps(self):
        """
        Initiate an GAP request to slinkgo server

        return -- A dictionary like object (collections.defaultdict).
        """
        self._init_connection()
        request = json.dumps({"Command": "gap"})
        self._send_request(request)
        response = self._recv_response()
        obj = json.loads(response)

        result = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: list()))))

        for channel_name in obj["gap"]:
            split_channel = channel_name.split('_')
            current_network = split_channel[0]
            current_station = split_channel[1]
            current_location = split_channel[2]
            current_channel = split_channel[3]

            # Begin to store gaps
            for current_gap in obj["gap"][channel_name]["PeriodList"]:
                # "2016-08-30T14:35:39.725Z"
                start = normalize_date(current_gap["StartTime"])
                begintime = datetime.strptime(start,
                                              '%Y-%m-%dT%H:%M:%S.%fZ')

                end = normalize_date(current_gap["EndTime"])
                endtime = datetime.strptime(end,
                                            '%Y-%m-%dT%H:%M:%S.%fZ')
                result[current_network][current_station][current_location][current_channel].append((begintime,
                                                                                                    endtime))

        for channel_name in obj["overlap"]:
            split_channel = channel_name.split('_')
            current_network = split_channel[0]
            current_station = split_channel[1]
            current_location = split_channel[2]
            current_channel = split_channel[3]

            # Now store overlaps
            for current_overlap in obj["overlap"][channel_name]["PeriodList"]:
                # "2016-08-30T14:35:39.725Z"
                start = normalize_date(current_overlap["StartTime"])
                begintime = datetime.strptime(start,
                                              '%Y-%m-%dT%H:%M:%S.%fZ')

                end = normalize_date(current_overlap["EndTime"])
                endtime = datetime.strptime(end,
                                            '%Y-%m-%dT%H:%M:%S.%fZ')
                result[current_network][current_station][current_location][current_channel].append((endtime,
                                                                                                    begintime))

        return result


class SlinkgoClient(object):

    """
    Initialize this SlinkgoClient.

    address -- The slinkgo server address
    port    -- The slinkgo server port (default:1234)
    """
    def __init__(self, address, port=1234):
        self.__address = address
        self.__port = port

    def _init_connection(self):
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__sock.settimeout(5)
        self.__sock.connect((self.__address, self.__port))

    def _send_request(self, request):
        sent = 0
        while sent < len(request):
            sent += self.__sock.send(request[sent:])

    def _recv_response(self):
        """
        Read request response

        return -- The request response as string
        """
        data = ''
        dataTmp = self.__sock.recv(4096)
        while dataTmp:
            data += dataTmp
            dataTmp = self.__sock.recv(4096)
        return data

    def get_age(self, stream_list):
        """
        Initiate de age request to slinkgo server
        for a given stream list.

        stream_list -- The list of stream id (NN_SS_LL_CCC)

        return      -- A dictionnary like:
                       {
                         "dataage": {
                           <streamid>: <dataage in seconds>,
                           ...
                         },
                         "packetage": {
                           <streamid>: <packetage in seconds>,
                         }
                       }
        """
        self._init_connection()
        request = json.dumps({"Name": "age", "Parameter": stream_list})
        self._send_request(request)
        response = self._recv_response()
        obj = json.loads(response)
        return obj

    def get_stat(self):
        """
        Initiate an STAT request to slinkgo server

        return -- A dictionary.
        """
        self._init_connection()
        request = json.dumps({"Name": "stat"})
        self._send_request(request)
        response = self._recv_response()
        obj = json.loads(response)
        return obj

    def get_gaps(self, stream_list):
        """
        Initiate an GAP request to slinkgo server

        return -- A dictionary like object (collections.defaultdict).
        """
        self._init_connection()
        request = json.dumps({"Name": "gap", "Parameter": stream_list})
        self._send_request(request)
        response = self._recv_response()
        obj = json.loads(response)
        return obj

    def get_stream(self):
               """
        Initiate an STREAM request to slinkgo server

        return -- A dictionary like object (collections.defaultdict).
        """
        self._init_connection()
        request = json.dumps({"Name": "stream"})
        self._send_request(request)
        response = self._recv_response()
        obj = json.loads(response)
        return obj


if __name__ == "__main__":
    client = SlinkgoClient("ghanima.unice.fr")
    print(json.dumps(client.get_age(["FR_MORSI_00_HHE", "RA_PYLL_00_HNZ", "RA_PYLL_00_HNE", "RA_PYLL_00_HNN", "FR_MORSI_00_HHZ"]), indent=2, default=str))
