# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
import struct


class SecureTransfert(object):

    __metaclass__ = ABCMeta

    def send(self, message):
        """
        Send the given message.

        message -- The message to send
        """
        unicode_flag = False
        if type(message) == unicode:
            unicode_flag = True
            message = message.encode('utf8')
        message64 = message.encode('base64')
        frmt = "!%ds" % len(message64)
        packed_msg = struct.pack(frmt, message64)
        packed_hdr = struct.pack('!I?', len(packed_msg), unicode_flag)

        self._send(packed_hdr)
        self._send(packed_msg)

    @abstractmethod
    def _send(self, packet):
        pass

    def recv(self):
        """
        Receive the next available message.
        """
        header = self._recv(5)
        message_size, unicode_flag = struct.unpack('!I?', header)
        content = self._recv(message_size)
        frmt = '!%ds' % message_size
        (message64,) = struct.unpack(frmt, content)
        if unicode_flag:
            return message64.decode('base64').decode('utf8')
        else:
            return message64.decode('base64')

    @abstractmethod
    def _recv(self, size):
        pass


class SecureSocket(SecureTransfert):

    def __init__(self, connection):
        """
        Initialize this JSONSocket

        connection -- The connection used to send JSON object
        """
        self.__connection = connection

    def _send(self, packet):
        """
        Send a packet.

        packet -- The packet to send as string
        """
        self.__connection.sendall(packet)

    def _recv(self, size):
        """
        Read the given number of bytes from socket

        size -- The number of bytes to read
        """
        chunks = []
        nb_recv = 0
        while nb_recv < size:
            dataTmp = self.__connection.recv(size - nb_recv)
            if len(dataTmp) == 0:
                raise RuntimeError("socket connection broken")
            chunks.append(str(dataTmp))
            nb_recv += len(dataTmp)
        return str('').join(chunks)

    def close(self):
        """
        Close the socket associated with this SafeSocket
        """
        self.__connection.close()


class SecurePipe(SecureTransfert):
    def __init__(self, pipe_in, pipe_out):
        self.__pipe_in = pipe_in
        self.__pipe_out = pipe_out

    def _send(self, packet):
        self.__pipe_out.write(packet)

    def _recv(self, size):
        data = ''
        while len(data) < size:
            dataTmp = self.__pipe_in.read(size - len(data))
            data += dataTmp
            if dataTmp == '':
                raise RuntimeError("Reach EOF")
        return data
