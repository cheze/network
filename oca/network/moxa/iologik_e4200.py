#!/usr/bin/python
# -*- coding: utf-8 -*-
import cookielib
import urllib2
import  hashlib
import re


class ioLogikE4200(object):
    def __init__(self, ip, port, password=None):
        self.__ip = ip
        self.__port = port
        self.__password = password
        self.__opener = None
        self.__cookies = None
        self.__number_of_module = None
        self.__firmware_version = None
        self.__serial_number = None
        self.__modules = []
        self.__re_module_code = '^SLOT(\d{2})_MOXA=(\d{4})$'
        self.__re_channel_status = '^SLOT(\d{2})CH(\d{2})_STATUS=(\d+)$'

    def __str__(self):
        if not self.__modules:
            return 'MOXA\n - No module set'
        result = ['MOXA - S/N: %s - Firmware: %s' % (self.__serial_number,
                                                     self.__firmware_version)]
        for module in self.__modules:
            result.append(str(module))
        return '\n'.join(result)

    def __iter__(self):
        for module in self.__modules:
            yield module

    def _gen_pass_cookie_value(self):
        '''
        Generate the content of the MoxaPass cookie from
        the valid password.
        '''
        md5 = hashlib.new('md5')
        md5.update(self.__password)
        result = md5.digest()
        return ''.join(map(lambda x:'%02x' % ord(x),
                       result))
       
    def _get_cookies(self):
        '''
        Get and create session cookie used for authentication on
        data requests.
        '''
        if self.__cookies:
            return
        self.__cookies = cookielib.CookieJar()
        self.__opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.__cookies))
        self.__opener.open('http://%s:%s/' % (self.__ip, self.__port))
        for c in self.__cookies:
            if c.name == ('MoxaPass') and self.__password:
                c.value = self._gen_pass_cookie_value()

    def _gen_cgi_request(self, req_list):
        '''
        Generate a list of requests with a maximum of 200 character for each.
        '''
        req_prefix = 'http://%s:%s/getParam.cgi?' % (self.__ip, self.__port)
        max_length = 200 - len(req_prefix)
        result = []
        char_count = 0
        tmp_req_list = []
        for current_req in req_list:
            if (char_count + len(current_req)) > max_length:
                result.append(tmp_req_list)
                tmp_req_list = []
                char_count = 0
            tmp_req_list.append(current_req)
            char_count += len(current_req)
        if tmp_req_list:
            result.append(tmp_req_list)
        return map(lambda x:'%s%s' % (req_prefix, '&'.join(x)), result)

    def _load_info(self):
        '''
        Retrieve from the ioLogikE4200 general info like the number of modules, the
        firmware version and the serial number.
        '''
        req_list = ['NUM_SLOTS=?', 'FWR_V=?', 'SN_NUM=?']
        req_list = self._gen_cgi_request(req_list)
        self._get_cookies()
        for req in req_list:
            data = self.__opener.open(req).read().split('<br>')
            for value in data:
                if not value:
                    continue
                if value.startswith('FWR_V'):
                    self.__firmware_version = value.split('=')[1]
                elif value.startswith('SN_NUM'):
                    self.__serial_number = value.split('=')[1]
                elif value.startswith('NUM_SLOT'):
                    self.__number_of_module = int(value.split('=')[1])
                else:
                    raise ValueError("Bad response due to bad cookie value (?) : %s" % self.__cookies)

        # Init the list of module
        self.__modules = map(lambda x:None, xrange(self.__number_of_module))
 
        # Create request to retrieve the code of each module
        req_list = []
        for i in xrange(self.__number_of_module):
            req_list.append('SLOT%02d_MOXA=?' % i)
        req_list = self._gen_cgi_request(req_list)

        # Send the request(s)
        for req in req_list:
            data = self.__opener.open(req).read().split('<br>')
            for value in data:
                if not value:
                    continue
                m = re.match(self.__re_module_code, value)
                if m:
                    slot, code = m.groups()
                    # Create Module instance and append to the module list in the right index
                    self.__modules[int(slot)] = Module(code=code, slot=int(slot))

    def load_data(self):
        '''
        Retrieve the value of all available channels for each module.
        '''
        # Retrieve general info from the ioLogikE4200
        self._load_info()
 
        # Create request to retrieve each channel value for all modules
        req_list = []
        for module in self.__modules:
            for ch in xrange(module.get_number_of_channels()):
                req_list.append('SLOT%02dCH%02d_STATUS=?' %
                                (module.get_slot(), ch))
        req_list = self._gen_cgi_request(req_list)

        # Send the request(s)
        for req in req_list:
            data = self.__opener.open(req).read().split('<br>')
            for value in data:
                if not value:
                    continue
                m = re.match(self.__re_channel_status, value)
                if m:
                    slot, channel, val = m.groups()
                    # Affects the retrieve value to the right module and the right channel
                    self.__modules[int(slot)].set_channel_value(int(channel), val)

    def get_module(self, slot):
        return self.__modules[slot]
                   
 
class Module(object):
    modulesDescription = {
        '1800': (8, '8 digital inputs, sink, 24 VDC, removable terminal block'),
        '1801': (8, '8 digital inputs, source, 24 VDC, removable terminal block'),
        '1600': (16, '16 digital inputs, sink, 24 VDC, 20-pin header'),
        '1601': (16, '16 digital inputs, source, 24 VDC, 20-pin header'),
        '1450': (4, '4 digital inputs, sink, 110 VAC, removable terminal block'),
        '1451': (4, '4 digital inputs, sink, 220 VAC, removable terminal block'),
        '2450': (4, '4 relay outputs, 24-VDC/230-VAC, 2 A, removable terminal block'),
        '2800': (8, '8 digital outputs, sink, 24 VDC, 0.5A, removable terminal block'),
        '2801': (8, '8 digital outputs, source, 24 VDC, 0.5A, removable terminal block'),
        '2600': (16, '16 digital outputs, sink, 24 VDC, 0.5A, 20-pin header'),
        '2601': (16, '16 digital outputs, source, 24 VDC, 0.5A, 20-pin header'),
        '3402': (4, '4 analog inputs, 4 to 20mA, 12-bit, removable terminal block'),
        '3410': (4, '4 analog inputs, 0 to 10V, 12-bit, removable terminal block'),
        '3810': (8, '8 analog inputs, 0 to 10V, 12-bit, removable terminal block'),
        '6200': (2, '2 analog inputs, RTD, removable terminal block'),
        '6201': (2, '2 analog inputs, thermocouple, removable terminal block'),
        '4202': (2, '2 analog outputs, 4 to 20mA, 12-bit, removable terminal block'),
        '4210': (2, '2 analog outputs, 0 to 10V, 12-bit, removable terminal block'),
        '7001': (0, 'System expansion power supply, 1.0 A (5 VDC)'),
        '7002': (0, 'Field power distributor, 10A (24/48 VDC, 110/220 VAC)'),
        '7806': (0, 'Potential distributor, 8ch, 4ch / 24 VDC, 4ch / 0VDC')}
    def __init__(self, code=None, slot=None):
        self.__code = code
        self.__slot = slot
        self.__description = None
        self.__number_of_channels = None
        self.__channels_value = []
        if code:
            self.set_code(code)
    def set_code(self, code):
        self.__code = code
        try:
            self.__number_of_channels = Module.modulesDescription[code][0]
            self.__description = Module.modulesDescription[code][1]
            self.set_number_of_channels(self.__number_of_channels)
        except KeyError:
            raise ValueError('Unknown code of module : %s' % code)
    def set_slot(self, slot):
        self.__slot = slot
    def set_number_of_channels(self, number_of_channels):
        self.__number_of_channels = number_of_channels
        self.__channels_value = map(lambda x:None, xrange(number_of_channels))
    def set_channel_value(self, channel_number, value):
        self.__channels_value[channel_number] = value
    def get_code(self):
        return self.__code
    def get_slot(self):
        return self.__slot
    def get_description(self):
        return self.__description
    def get_number_of_channels(self):
        return self.__number_of_channels
    def get_raw_value(self, channel_number):
        return self.__channels_value[channel_number]
    def get_channel_value(self, channel_number):
        if self.get_code() == '1451':
            return self.__channels_value[channel_number] == "1"
        elif self.get_code() == '1800':
            return self.__channels_value[channel_number] == "1" 
        elif self.get_code() == '2450':
            return self.__channels_value[channel_number] == "1"
        elif self.get_code() == '2800':
            return self.__channels_value[channel_number] == "1"
        elif self.get_code() == '3810':
            return (int(self.__channels_value[channel_number])*10.0)/4096
        elif self.get_code() == '6200':
            return int(self.__channels_value[channel_number])/10.0
        
        else:
            raise ValueError("Module not yet managed (%s)." % self.get_code())
    def __str__(self):
        result = []
        result.append(' SLOT%02d - %s => [ %s ]' % (self.get_slot(), self.get_code(),
                                                    ', '.join(map(lambda x:'%5s' % self.get_channel_value(x),
                                                              xrange(self.get_number_of_channels())))))
        #result.append('           \\_ (%s)' % self.get_description())
        return '\n'.join(result)            
    
            

def main():
    from datetime import datetime, timedelta
    from time import sleep
    #m = ioLogikE4200('80.14.250.56', 83, 'root_tmp') #NCAD
    #m = ioLogikE4200('80.15.9.190', 83, 'roottmp') #TURF
    m = ioLogikE4200('blafisdn.dyndns.org', 83, 'roottmp') #BLAF
    sample_period = timedelta(seconds=5)
    expected_time = datetime.utcnow()
    while True:
        t0 = datetime.utcnow()
        next_expected_time = expected_time + sample_period
        print('\n%s' % t0)
        m.load_data()
        print(m)
        print("TEST: SLOT01CH00=%s" % m.get_module(1).get_channel_value(0))
        t1 = datetime.utcnow()
        time2sleep = sample_period - (t0 - expected_time) - (t1 - t0)
        expected_time = next_expected_time
        sleep(time2sleep.seconds + time2sleep.microseconds*10**(-6))

if __name__ == '__main__':
    main()
