#!/usr/bin/python27
# -*- coding: utf-8 -*-
from obspy.clients.seedlink import SLClient
from collections import defaultdict
from datetime import datetime
from xml.sax import ContentHandler, parseString
import socket
import json
import sys
import os
import re


class InfoGapsHandler(ContentHandler):
    '''
    XML handler used to retrieve information from an INFO GAPS XML response.
    '''
    def __init__(self):
        ContentHandler.__init__(self)
        self.__current_network = ''
        self.__current_station = ''
        self.__current_location = ''
        self.__current_channel = ''
        self.__result = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: list()))))

    def startElement(self, name, attrs):
        if name == 'station':
            self.__current_network = attrs.getValue('network')
            self.__current_station = attrs.getValue('name')
        elif name == 'stream':
            self.__current_location = attrs.getValue('location')
            self.__current_channel = attrs.getValue('seedname')
        elif name == 'gap':
            begintime = datetime.strptime('%s00 UTC' % attrs.getValue('begin_time'),
                                          '%Y/%m/%d %H:%M:%S.%f %Z')
            endtime = datetime.strptime('%s00 UTC' % attrs.getValue('end_time'),
                                        '%Y/%m/%d %H:%M:%S.%f %Z')
            self.__result[self.__current_network][self.__current_station][self.__current_location]\
                         [self.__current_channel].append((begintime,
                                                          endtime))

    def get_result(self):
        '''
        Return a dict-like object (collections.defaultdict) in the following structure :
        {'<network>': {
            '<station>':{
                '<location>': {
                    '<channel>': [(datetime.datetime(<begin_time>),
                                  datetime.datetime(<end_time>))]
                    }
                }
            }
        }
        '''
        return self.__result


class InfoStreamsHandler(ContentHandler):
    '''
    XML handler used to retrieve information from an INFO STREAMS XML response.
    '''
    def __init__(self):
        ContentHandler.__init__(self)
        self.__current_network = ''
        self.__current_station = ''
        self.__current_location = ''
        self.__current_channel = ''
        self.__result = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: None))))

    def startElement(self, name, attrs):
        if name == 'station':
            self.__current_network = attrs.getValue('network')
            self.__current_station = attrs.getValue('name')
        elif name == 'stream':
            self.__current_location = attrs.getValue('location')
            self.__current_channel = attrs.getValue('seedname')
            begintime = datetime.strptime('%s00 UTC' % attrs.getValue('begin_time'),
                                          '%Y/%m/%d %H:%M:%S.%f %Z')
            endtime = datetime.strptime('%s00 UTC' % attrs.getValue('end_time'),
                                        '%Y/%m/%d %H:%M:%S.%f %Z')
            self.__result[self.__current_network][self.__current_station]\
                         [self.__current_location]\
                         [self.__current_channel] = (begintime,
                                                     endtime)

    def get_result(self):
        '''
        Return a dict-like object (collections.defaultdict) in the following structure :
        {'<network>': {
            '<station>':{
                '<location>': {
                    '<channel>': (datetime.datetime(<begin_time>),
                                  datetime.datetime(<end_time>))
                    }
                }
            }
        }
        '''
        return self.__result


def get_info_streams(ip, port=18000):
    '''
    Initiate an INFO STREAMS request to a remote server
    and parse the XML response.

    ip     -- The IP address of the remote SeedLink server
    port   -- The port of the remote SeedLink server

    return -- A dictionary like object (collections.defaultdict).
              The value of a non-present stream is None.
    '''
    save_stdout = sys.stdout
    sys.stdout = open(os.devnull, 'w')
    try:
        sl_client = SLClient(loglevel='CRITICAL')
        sl_client.slconn.set_sl_address("%s:%s" % (ip, port))
        sl_client.infolevel = 'STREAMS'
        sl_client.initialize()
        sl_client.run()
    finally:
        sys.stdout = save_stdout

    infostreams_str = sl_client.slconn.get_info_string().replace('\x00', '')
    handler = InfoStreamsHandler()
    parseString(infostreams_str, handler)

    return handler.get_result()


DATE_WITHOUT_NANO_RE = '([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{6})'
DATE_WITHOUT_MICRO = '([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z)'


def normalize_date(unormalized_date):
    m = re.match(DATE_WITHOUT_NANO_RE, unormalized_date)
    if m:
        return "%sZ" % m.groups(0)
    m = re.match(DATE_WITHOUT_MICRO, unormalized_date)
    if m:
        return "%s.0Z" % m.groups()[0][:-1]
    return unormalized_date


def get_info_gaps(ip, port=18000):
    '''
    Initiate an INFO GAPS request to a remote server
    and parse the XML response.

    ip     -- The IP address of the remote SeedLink server
    port   -- The port of the remote SeedLink server

    return -- A dictionary like object (collections.defaultdict).
    '''
    sock = socket.socket()
    sock.connect(("127.0.0.1", 1234))
    request = json.dumps({"Command": "gap"})

    # Send request
    sent = 0
    while sent < len(request):
        sent += sock.send(request[sent:])
    # Get result
    data = ''
    dataTmp = sock.recv(4096)
    while dataTmp:
        data += dataTmp
        dataTmp = sock.recv(4096)
    obj = json.loads(data)

    result = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: list()))))

    for channel_name in obj:
        split_channel = channel_name.split('_')
        current_network = split_channel[0]
        current_station = split_channel[1]
        current_location = split_channel[2]
        current_channel = split_channel[3]
        for current_gap in obj[channel_name]:
            # "2016-08-30T14:35:39.725Z"
            start = normalize_date(current_gap["StartTime"])
            begintime = datetime.strptime(start,
                                          '%Y-%m-%dT%H:%M:%S.%fZ')

            end = normalize_date(current_gap["EndTime"])
            endtime = datetime.strptime(end,
                                        '%Y-%m-%dT%H:%M:%S.%fZ')
            result[current_network][current_station]\
                  [current_location]\
                  [current_channel].append((begintime,
                                            endtime))

    return result


if __name__ == '__main__':
    infogaps = get_info_gaps("localhost", 1234)
    print(infogaps)
